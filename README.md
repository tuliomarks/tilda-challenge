# Tilda Fullstack Quiz Challenge

This project was developed with:

- React
- Tailwind CSS (CSS framework)
- Hasura Graph API for dynamic data
- Jest (for tests)

## Available Scripts
In the project directory, you can run:

### `npm start`
Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
  
### `npm test`
Launches the test runner in the interactive watch mode.

## Open Questions

### If you wanted to persist user answers in the database how would you do so?  Do you see any other points that could be improved in our data model? If yes, what changes would you make?
I see two different opportunities: 
- One solution is to create a new step at the end of the questionary, opening a new page with a name input and submit button. It Will be needed to save a hash for this user, with a name, quiz id and final score on Graphql API when the submit button is pressed, the user id could also be saved into the browser's storage in case of refreshes. When the QuizList is rendered the user's data could be requested to Graphql API.
- Another solution is to create an authentication feature for this app, which will be needed a new table for the users and another for the answer for each quiz and user. To do it will create the authentication components, also need to change the QuizForm component, and on "handleSubmit" to do a Graphql call to save the data. 

A change could be done in the data model by adding an order column to the question's relation table. The Graphql API could result in different question orders without an order by clause defined.

### Please describe how would you deploy your application and which services you would use. Please also describe which factors you would consider for your decision, like scalability, security, etc.
Based on previous experience I would choose the AWS services because are reliable, have a lot of service options and machine configurations to provide scalability and security. This application could be hosted into an S3 bucket with the Static Hosting feature. The API could be migrated to AWS Lambda (or EC2) + DynamoDB also.

### For this exercise, we used Hasura engine as a GraphQL Server generator. Imagine that you were to develop this Quiz in the real world. How would you develop your backend? Please describe which tools and what your application architecture would look like.
That was my first time consuming and API with GraphQL, seems flexible, simple like SQL, but I don't know the effort to maintain this API and extend it. With that, I would choose to use a Node.js server, using Typescript, with Mysql database, that option is interesting because the language will be TS (or JS) for front and backend. Another alternative could be a C# (.Net Core) backend with SQLServer or Mysql.

For the application strutucture, I would choose to do it as a microservice, start with one API for Quiz and Question, and another for Authentication for example.