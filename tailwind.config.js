module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'tilda-green': '#0c826b',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
