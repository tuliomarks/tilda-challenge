import React from "react";

const QuizCard = ({ quiz, score, onStartClicked }) => {
  const handleStartClick = () => onStartClicked(quiz);
  const { name, questions } = quiz;

  return (
    <div className="flex flex-col p-4 mb-4 sm:ml-4 border rounded-md sm:w-1/2 md:w-1/4 w-full">
      <h3 className="text-lg font">{name}</h3>
      <div className="flex flex-row justify-between items-center">
        <p className="text-gray-500">
          Score {score || 0}/{questions?.length || 0}
        </p>
        <button className="btn-primary" onClick={handleStartClick}>
          {score !== null ? "Redo" : "Start"}
        </button>
      </div>
    </div>
  );
};

export default QuizCard;
