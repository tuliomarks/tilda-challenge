import React, { useState, useEffect } from "react";
import { QuizCard } from ".";
import { QuizService } from "../../Services";

const QuizList = ({ scoreList, onStartClicked }) => {
  const [quizzes, setQuizzes] = useState([]);

  useEffect(() => {
    QuizService.getQuizzes().then((result) => {
      setQuizzes(result);
    });
  }, []);

  const getCurrentQuizScore = (quiz) => {
    const currentQuizScore = scoreList.find(
      (score) => score.quizId === quiz.id
    );
    return currentQuizScore !== undefined ? currentQuizScore.finalScore : null;
  };

  return (
    <div className="flex sm:flex-row flex-col justify-center m-4">
      {quizzes.map((quiz) => (
        <QuizCard
          key={quiz.id}
          quiz={quiz}
          score={getCurrentQuizScore(quiz)}
          onStartClicked={onStartClicked}
        ></QuizCard>
      ))}
    </div>
  );
};

export default QuizList;
