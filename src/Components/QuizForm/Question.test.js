import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import { Question } from '.';
import { questionMock } from "../../Mocks/Question.mock";

it("On Question component will render the title and options", () => {

  const question = questionMock[0];

  render(<Question question={question} />);

  expect(screen.getByTestId("title")).toBeInTheDocument();
  expect(screen.getByTestId("title")).toContainHTML(question.text);

  const options = screen.getAllByTestId(/option-[0-9]+/);

  expect(options.length).toBe(4);
});