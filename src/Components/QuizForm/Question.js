import React from "react";

const Question = ({ question, onAnswerSelected, onClosed }) => {
  const { id, text, options } = question;

  const optionsList = options.split(",");

  return (
    <div className="flex flex-col p-4 mb-4 border rounded-md" data-testid={`question-${id}`}>
      <div className="flex flex-row mb-4">
        <button className="btn mr-4" onClick={onClosed}>
          <img className="w-4" src="back_icon.svg" />
        </button>
        <h3 className="text-lg font" data-testid="title">
          {text}
        </h3>
      </div>
      <div className="flex flex-col">
        {optionsList.map((option, optionIndex) => (
          <label
            className="border rounded-md p-2 mb-2"
            key={`option-${optionIndex}`}
            data-testid={`option-${optionIndex}`}
          >
            <input
              className="mr-2"
              type="radio"
              name="question-option"
              value={option}
              onChange={() => onAnswerSelected(option)}
            />
            {option}
          </label>
        ))}
      </div>
    </div>
  );
};

export default Question;
