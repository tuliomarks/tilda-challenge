import React, { useState, useRef } from "react";
import { Question } from ".";
import { QuizService } from "../../Services";

const QuizForm = ({ quiz, onQuestionsEnded }) => {

  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);

  const { questions } = quiz;
  const currentQuestion = questions[currentQuestionIndex];
  const answersList = useRef([]);

  const handleBack = () => {
    if (currentQuestionIndex === 0) return;

    setCurrentQuestionIndex(currentQuestionIndex - 1);
  };

  const handleNext = () => setCurrentQuestionIndex(currentQuestionIndex + 1);
  const handleSubmit = () => {
    if (!onQuestionsEnded) return;

    const quizScore = QuizService.calculateScore(quiz.id, answersList.current);
    onQuestionsEnded(quizScore);
  };

  const handleOnClosed = () => onQuestionsEnded(null);

  const handleAnswer = (userAnswer) => {
    const newValue = QuizService.createAnswer(currentQuestion, userAnswer);

    // replace or insert the new answer on the answerList
    const oldIndex = answersList.current.findIndex(
      (x) => x.QuestionId === currentQuestion.id
    );
    if (oldIndex !== -1) {
      answersList.current[oldIndex] = newValue;
    } else {
      answersList.current.push(newValue);
    }
  };

  return (
    <div className="flex flex-col mx-auto m-4 md:max-w-screen-sm">
      <Question
        key={currentQuestion.id}
        question={currentQuestion}
        onAnswerSelected={handleAnswer}
        onClosed={handleOnClosed}
      />
      <div className="flex justify-center">
        <button
          data-testid="btn-back"
          className="btn mr-4"
          onClick={handleBack}
          disabled={currentQuestionIndex === 0}
        >
          Back
        </button>
        {currentQuestionIndex + 1 < questions.length ? (
          <button
            data-testid="btn-next"
            className="btn-primary"
            onClick={handleNext}
          >
            Next
          </button>
        ) : (
          <button
            data-testid="btn-submit"
            className="btn-primary"
            onClick={handleSubmit}
          >
            Submit
          </button>
        )}
      </div>
    </div>
  );
};

export default QuizForm;
