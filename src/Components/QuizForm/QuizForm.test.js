import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import { QuizForm } from '.';
import { quizMock } from "../../Mocks/Quiz.mock";

it("On QuizForm component will render the buttons back and next", () => {

  const quiz = quizMock[0];

  render(<QuizForm quiz={quiz} />);

  expect(screen.getByTestId(`btn-back`)).toBeInTheDocument();
  expect(screen.getByTestId(`btn-next`)).toBeInTheDocument();
});

it("On QuizForm component will render only one question", () => {

  const quiz = quizMock[0];
  const firstQuestion = quiz.questions[0];

  render(<QuizForm quiz={quiz} />);

  const questions = screen.getAllByTestId(/^question-/);
  expect(questions.length).toBe(1);
  expect(screen.getByTestId(`question-${firstQuestion.id}`)).toBeInTheDocument();

});

it("On QuizForm component will disable the back button on first question", () => {

  const quiz = quizMock[0];

  render(<QuizForm quiz={quiz} />);

  expect(screen.getByTestId(`btn-back`)).toBeDisabled();

});

it("On QuizForm component will render submit button on the last question, instead next button", () => {

  const quiz = quizMock[0];

  render(<QuizForm quiz={quiz} />);

  const btnNext = screen.getByTestId(`btn-next`);
  btnNext.click();
  btnNext.click();
  btnNext.click();

  expect(screen.getByTestId(`btn-submit`)).toBeInTheDocument();
  expect(screen.queryByTestId(`btn-next`)).not.toBeInTheDocument();
});