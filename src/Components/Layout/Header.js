import React from "react";

const Header = () => {
  return (
      <header className="flex items-center justify-center flex-wrap bg-tilda-green p-6" >
        <a className="font-bold text-xl text-white" href="https://tilda.de" target="_blank" rel="noreferrer">
          Tilda Quizz
        </a>
      </header>
  );
}

export default Header;
