import React, { useState } from "react";
import { Header } from ".";
import { QuizList } from "../QuizList";
import { QuizForm } from "../QuizForm";

const Layout = () => {
  const [currentQuiz, setCurrentQuiz] = useState(null);
  const [scoreList, setScoreList] = useState([]);

  const handleStartQuiz = (quiz) => {
    setCurrentQuiz({ ...quiz });
  };

  const handleQuizAnswerSubmit = (finalScore) => {
    setCurrentQuiz(null);

    if (!finalScore) return;

    const newList = scoreList.filter(
      (quizScore) => quizScore.quizId !== finalScore.quizId
    );

    setScoreList([...newList, finalScore]);
  };

  return (
    <>
      <Header></Header>
      <div className="container mx-auto">
        {currentQuiz === null ? (
          <QuizList scoreList={scoreList} onStartClicked={handleStartQuiz} />
        ) : (
          <QuizForm
            quiz={currentQuiz}
            onQuestionsEnded={handleQuizAnswerSubmit}
          />
        )}
      </div>
    </>
  );
};

export default Layout;
