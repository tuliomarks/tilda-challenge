import quizMock from './Quiz.mock';
import questionMock from './Question.mock';

export { quizMock, questionMock };