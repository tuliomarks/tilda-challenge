export const quizMock = [
    {
      "id": "03daf064-0b47-40c5-9b39-0d81a5f58397",
      "name": "Geography",
      "questions": [
        {
          "id": "558f8dd7-fc74-43ce-a13a-e0f88003dc30",
          "options": "Delhi,Tokyo,Paris,Sao Paulo",
          "answer": "Tokyo",
          "quiz_id": "03daf064-0b47-40c5-9b39-0d81a5f58397",
          "text": "What is the largest city in the world?"
        },
        {
          "id": "ff25a48d-589f-4708-96fb-2542f893bcb2",
          "options": "Peru,Bolivia,Columbia,Chile",
          "answer": "Peru",
          "quiz_id": "03daf064-0b47-40c5-9b39-0d81a5f58397",
          "text": "In what country can you visit Machu Picchu?"
        },
        {
          "id": "419bd976-3ce1-4d71-b3ec-17f1f2ee3bd3",
          "options": "35,48,54,25",
          "answer": "54",
          "quiz_id": "03daf064-0b47-40c5-9b39-0d81a5f58397",
          "text": "How many countries are there in Africa?"
        }
      ]
    },
    {
      "id": "84a4fd17-4f81-489f-bb96-84dfd71b93a6",
      "name": "General knowledge",
      "questions": [
        {
          "id": "2d567936-ece0-43c3-abc8-d51d29a24233",
          "options": "500,60000,1300,20000",
          "answer": "20000",
          "quiz_id": "84a4fd17-4f81-489f-bb96-84dfd71b93a6",
          "text": "How many breaths does the human body take daily?"
        },
        {
          "id": "7996abe0-6a48-47d7-9da2-c4e817387d1a",
          "options": "1968,1972,1989,2001",
          "answer": "1972",
          "quiz_id": "84a4fd17-4f81-489f-bb96-84dfd71b93a6",
          "text": "In which year was The Godfather first released?"
        },
        {
          "id": "1a843652-d1ee-4ef6-995e-901a9af7a148",
          "options": "Father McKenzie,Father McDonald,Father McCarthy,Father McCauley",
          "answer": "Father McKenzie",
          "quiz_id": "84a4fd17-4f81-489f-bb96-84dfd71b93a6",
          "text": "In \"Eleanor Rigby\" by The Beatles, what was the name of the priest?"
        }
      ]
    }
  ];