export const questionMock = [
    {
        "id": "2d567936-ece0-43c3-abc8-d51d29a24233",
        "options": "500,60000,1300,20000",
        "answer": "20000",
        "quiz_id": "84a4fd17-4f81-489f-bb96-84dfd71b93a6",
        "text": "How many breaths does the human body take daily?"
    },
];