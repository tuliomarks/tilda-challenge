import "@testing-library/jest-dom/extend-expect";

import { QuizService } from '.';
import { quizMock } from "../Mocks/Quiz.mock";
import { questionMock } from "../Mocks/Question.mock";

it("On QuizService will create an answer with QuestionId and isCorrect props", () => {

  const question = questionMock[0];
  const answer = QuizService.createAnswer(question, "20000");

  expect(answer).toHaveProperty("QuestionId");
  expect(answer).toHaveProperty("isCorrect");

});

it("On QuizService will create an answer with isCorrect = true on a correct user answer", () => {

  const question = questionMock[0];
  const answer = QuizService.createAnswer(question, "20000");

  expect(answer.isCorrect).toBe(true);

});

it("On QuizService will create an answer with isCorrect = false on a incorrect user answer", () => {

  const question = questionMock[0];
  const answer = QuizService.createAnswer(question, "1000");

  expect(answer.isCorrect).toBe(false);

});

it("On QuizService will calculate the quiz final score returning an object with quizId and finalScore", () => {

  const quiz = quizMock[0];
  const answers = [ { isCorrect: true }, { isCorrect: true }, { isCorrect: false }, { isCorrect: false } ];
  const quizResult = QuizService.calculateScore(quiz, answers);

  expect(quizResult).toHaveProperty("quizId");
  expect(quizResult).toHaveProperty("finalScore");

});

