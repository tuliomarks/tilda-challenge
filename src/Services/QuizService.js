import { gql, ApolloClient, InMemoryCache } from "@apollo/client";

const { REACT_APP_API_URL: API_URL } = process.env;

const client = new ApolloClient({
  uri: API_URL,
  cache: new InMemoryCache(),
});

const QuizService = {};

QuizService.createAnswer = (question, userAnswer) => {
  const isCorrect = question.answer === userAnswer;

  return { QuestionId: question.id, isCorrect };
};

QuizService.calculateScore = (quizId, answers) => {
  const obj = { quizId: quizId, finalScore: 0 };
  if (!answers) {
    return obj;
  }

  const correctAnswers = answers.filter((answer) => answer.isCorrect);

  obj.finalScore = correctAnswers.length;

  return obj;
};

QuizService.getQuizzes = () => {
  return client
    .query({
      query: gql`
        query MyQuery {
          quizzes {
            id
            name
            questions(order_by: { id: asc }) {
              id
              answer
              options
              text
            }
          }
        }
      `,
    })
    .then((response) => {
      return response?.data?.quizzes ?? [];
    });
};

export default QuizService;
